#!/usr/bin/python

import numpy
import numpy.linalg
import pyrr
import sys

if len(sys.argv) < 2:
  print("Usage: %s reference.xyz fitted.xyz > fitted_aligned.xyz" % sys.argv[0], file=sys.stderr)
  exit()


# Get coordinates from files

def read_xyz(filename):
  atom_names = []
  coords = []
  with open(filename) as f:
    next(f); next(f)
    for line in f:
      chunks = line.split()
      atom_names.append(chunks[0])
      coords.append([float(raw_coord) for raw_coord in chunks[1:]])
  return atom_names, numpy.array(coords, dtype=numpy.float_)

atom_names_a, coords_a = read_xyz(sys.argv[1])
atom_names_b, coords_b = read_xyz(sys.argv[2])


# Sanity check

if (len(coords_a) != len(coords_b)):
  print("Warning! Each molecule has a different number of atoms.\nTruncating the longer one.", file=sys.stderr)

atom_count = min(len(coords_a), len(coords_b))


# Matrices derived by substraction of mean (x' and y')

mean_a = numpy.mean(coords_a, axis=0)
mean_b = numpy.mean(coords_b, axis=0)

subtracted_a = numpy.subtract(coords_a, mean_a)
subtracted_b = numpy.subtract(coords_b, mean_b)


output_mol = subtracted_b


# Skew matrices A_k

skew_matrices = []

for i in range(atom_count):
  a = subtracted_b[i] + subtracted_a[i]
  b = subtracted_b[i] - subtracted_a[i]
  
  skew_matrix = numpy.array([ \
    [   0, -b[0], -b[1], -b[2]], \
    [b[0],     0, -a[2],  a[1]], \
    [b[1],  a[2],     0, -a[0]], \
    [b[2], -a[1],  a[0],     0] \
  ])

  skew_matrices.append(skew_matrix)


# Symmetric matrix B

B = numpy.mean([numpy.dot(numpy.transpose(matrix), matrix)  for matrix in skew_matrices], axis=0)


# Rotation

eigenvalues, eigenvectors = numpy.linalg.eigh(B)
smallest = eigenvectors[:, numpy.argmin(eigenvalues)]

print("Smallest eigenvalue λ_0 equals %.12f" % min(eigenvalues), file=sys.stderr)

rotation = pyrr.quaternion.create(w=smallest[0], x=smallest[1], y=smallest[2], z=smallest[3])
rotation = pyrr.quaternion.inverse(rotation)

output_mol = [pyrr.quaternion.apply_to_vector(rotation, output_mol[i])  for i in range(atom_count)]


# Translation 

translation = mean_a - numpy.average(output_mol, axis=0)
output_mol = [(coords + translation) for coords in output_mol]


# Output xyz file

print(atom_count)
print("rotated")
for i in range(atom_count):
  print("%s %f %f %f" % (atom_names_b[i], output_mol[i][0], output_mol[i][1], output_mol[i][2]))
