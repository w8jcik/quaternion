#!/usr/bin/bash
babel -ipdb tests/azurine/1cuo.ent -oxyz tmp/1cuo.xyz
babel -ipdb tests/azurine/1uat.ent -oxyz tmp/1uat.xyz
python fit.py tmp/1cuo.xyz tmp/1uat.xyz > tmp/1uat_fitted.xyz
vmd -e tests/azurine/azurine.tcl
