mol new {tmp/1cuo.xyz} type {xyz}
mol modselect 0 0 backbone
mol modcolor 0 0 ColorID 7

mol new {tmp/1uat.xyz} type {xyz}
mol modselect 0 1 backbone
mol modcolor 0 1 ColorID 1

mol new {tmp/1uat_fitted.xyz} type {xyz}
mol modselect 0 2 backbone
mol modcolor 0 2 ColorID 3
