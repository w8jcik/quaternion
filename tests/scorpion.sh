#!/usr/bin/bash
babel -ipdb tests/scorpion/1pnh.pdb -oxyz tmp/1pnh.xyz -f0 -l1
vmd tmp/1pnh.xyz -e tests/scorpion/rotate.tcl -dispdev text 
python fit.py tmp/1pnh.xyz tmp/1pnh_rotated.xyz > tmp/1pnh_rotated_fitted.xyz
vmd -e tests/scorpion/scorpion.tcl
