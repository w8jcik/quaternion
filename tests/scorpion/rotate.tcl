set sel [atomselect top "all"] 
set com [measure center $sel weight mass] 

set matrix [transaxis z 15] 
$sel move $matrix 
$sel moveby $com

set matrix [transaxis x 24] 
$sel move $matrix 
$sel moveby $com

set matrix [transaxis y 18] 
$sel move $matrix 
$sel moveby $com

$sel writexyz {tmp/1pnh_rotated.xyz}
exit
